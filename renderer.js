// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

//TODO https://konvajs.org/docs/index.html

const path = require('path');
const fs = require('fs');
const {remote} = require('electron')
const {Menu, MenuItem} = remote;

const root = fs.readdirSync('/');

const CollisionBoxCanvasHandler = require('./collision-box-canvas-handler.js');

const utils = require('./utils.js');


// This will print all files at the root-level of the disk,
// either '/' or 'C:\'.
console.log(root)

dialog = remote.dialog,
WIN = remote.getCurrentWindow();

let hitboxGeneratorUtility = {
	
	collisionBoxCanvasHandler : null,
	
	exportFilePath :'',
	
	saveFilePath : '',
		
	init : function() {
		
		//new
		$('#chooseAnimationRootFile').click(function() {
			hitboxGeneratorUtility.showDialog();			
		});
		
		$('#open').click(function() {
			//open a collisionBoxFile
			let options = {
				properties:["openFile"],
				filters: [
					{ name: 'Collision Box Data Files', extensions: ['collisionbox'] },
				]
				
			}
			
			//Synchronous
			let filePaths = dialog.showOpenDialog(WIN, options).then( (selectedFilePromise)=> {
				//open file and read into json Object
				
				let dataFilePath = selectedFilePromise.filePaths[0];
				
				fs.readFile(dataFilePath, 'utf-8', (err, data) => {
					if(err){
						alert("An error ocurred reading the file :" + err.message);
						return;
					}

					// Change how to handle the file content
					console.log("The file content is : " + data);
		
					let dataJson = JSON.parse(data);
					let path = dataJson.path;
		
					
					hitboxGeneratorUtility.onSpriteFolderSelected(path, dataJson);	
					
				});
				
			});
		});
		
		$('#animations').change(function() {
			
			let newAnimationName = $('#animations').val();
						
			//show selected animation (first frame)			
			hitboxGeneratorUtility.collisionBoxCanvasHandler.showFrame(newAnimationName, 0);	
			
			//show corresponding panels
			hitboxGeneratorUtility.showFramePanel(newAnimationName, 0);

			
			//reset frames options
			hitboxGeneratorUtility.resetFramesSelect(hitboxGeneratorUtility.currentSprite.animations[newAnimationName]);
			
			
			//replace info in animation info panel
			let fps = hitboxGeneratorUtility.collisionBoxCanvasHandler.getFrameRateOnCurrentAnimation();
			
			if (!fps) {
				hitboxGeneratorUtility.collisionBoxCanvasHandler.setFrameRateOnCurrentAnimation(12);
			}
			
			let isLooping = '' + hitboxGeneratorUtility.collisionBoxCanvasHandler.getIsLoopingOnCurrentAnimation();
			
			if (isLooping !== 'false' && isLooping !== 'true') {
				hitboxGeneratorUtility.collisionBoxCanvasHandler.setIsLoopingOnCurrentAnimation(false);
				isLooping = 'false';
					
			}
			
			$('#fps').val(fps);
			$('#isLooping').val(isLooping);

		});
		
		$('#frames').change(function() {
						
			let currentAnimationName = $('#animations').val();

			let newFrameNumber = $('#frames').val();
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

			collisionBoxCanvasHandler.showFrame(currentAnimationName, newFrameNumber);
						
			//show corresponding panels
			hitboxGeneratorUtility.showFramePanel(currentAnimationName, newFrameNumber);
		});
		
		$('#offsetX').change(function() {
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

			let valString = $('#offsetX').val();
			let val = parseInt(valString);
			if (Number.isNaN(val))
			{
				val = 0;
				$('#offsetX').val(0);
			}
			
			collisionBoxCanvasHandler.setOffsetX(val);
		});
		
		$('#offsetY').change(function() {
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

			let valString = $('#offsetY').val();
			let val = parseInt(valString);
			if (Number.isNaN(val))
			{
				val = 0;
				$('#offsetY').val(0);
			}
			
			collisionBoxCanvasHandler.setOffsetY(val);

		});
		
		$('#boundsWidth').change(function() {
			let valString = $('#boundsWidth').val();
			let val = parseInt(valString);
			if (Number.isNaN(val))
			{
				val = 0;
				$('#boundsWidth').val(0);
			}
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

			collisionBoxCanvasHandler.setBoundingBoxWidth(val);
			//hitboxGeneratorUtility.draw();
		});

		$('#boundsHeight').change(function() {
			let valString = $('#boundsHeight').val();
			let val = parseInt(valString);
			if (Number.isNaN(val))
			{
				val = 0;
				$('#boundsHeight').val(0);
			}
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

			collisionBoxCanvasHandler.setBoundingBoxHeight(val);
			//hitboxGeneratorUtility.draw();
		});		
		
		
		$('#addCollisionBox').click(function() {
			//hitboxGeneratorUtility.createHitBox();
			console.log('Adding Hitbox');
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

			collisionBoxCanvasHandler.createCollisionBox();
		});
		
		/*
		//add to toolbar
		const menu = new Menu();

         // Build menu one item at a time, unlike
         menu.append(new MenuItem ({
            label: 'File',
			submenu: [
				{
					label: "New",
					click() { 
						hitboxGeneratorUtility.onNewPressed();
					}
				},
				{
					label: "Open",
					click() { 
						hitboxGeneratorUtility.onOpenPressed();
					}
				},
				{
					label: "Save",
					click() { 
						hitboxGeneratorUtility.onSavePressed();
					}
				},				
				{
					label: "Export",
					click() { 
						hitboxGeneratorUtility.onExportPressed();
					}
				}				
			]
            
         }));
		 
		 Menu.setApplicationMenu(menu); */

		
		//move these to toolbar
		$('#saveButton').click(function() {
						
			//if already has save path
			if (hitboxGeneratorUtility.saveFilePath)
			{
				hitboxGeneratorUtility.save();
			}
			
			else
			{
				hitboxGeneratorUtility.saveAs();
			}
			
		});
		
		//move these to tool bar
		$('#exportButton').click(function() {
			//if already has save path
			if (hitboxGeneratorUtility.exportFilePath)
			{
				hitboxGeneratorUtility.export();
			}
			
			else
			{
				hitboxGeneratorUtility.exportAs();
			}
		});
		
		this.setUpAnimationInfoPanel();
		
		
	},
	
	onSavePressed : function() {
		//if already has save path
		if (hitboxGeneratorUtility.saveFilePath)
		{
			hitboxGeneratorUtility.save();
		}
		
		else
		{
			hitboxGeneratorUtility.saveAs();
		}
	},
	
	saveAs : function() {
		// Resolves to a Promise<Object>
		dialog.showSaveDialog({
			title: 'Select the File Path to save',
			defaultPath: path.join(__dirname, ''),  //default path should be existing if it exists
			buttonLabel: 'Save',
			// Restricting the user to only Collision Box Generator Files.
			filters: [
				{
					name: 'Collision Box Generator Files',
					extensions: ['collisionbox']
				}, ],
			properties: []
		}).then(file => {
			// Stating whether dialog operation was cancelled or not.
			console.log(file.canceled);
			if (!file.canceled) {
				console.log(file.filePath.toString());
				
				hitboxGeneratorUtility.saveFilePath = file.filePath.toString();
				  
				hitboxGeneratorUtility.save();
			}
		}).catch(err => {
			console.log(err)
		});
	},
		
	save : function() {

		//create save file
		let saveJson = hitboxGeneratorUtility.collisionBoxCanvasHandler.createSaveJson();
		
		if (hitboxGeneratorUtility.saveFilePath)
		{				  
			// Creating and Writing to the sample.txt file
			fs.writeFile(hitboxGeneratorUtility.saveFilePath, saveJson, function (err) {
				if (err) throw err;
				console.log('Saved!');
			});
		}

		else
		{
			console.log(saveJson);
			throw "ERROR! NO SAVE PATH FOUND!!";
		}			
	},
	
	exportAs : function() {
		// Resolves to a Promise<Object>
		dialog.showSaveDialog({
			title: 'Select the File Path to save',
			defaultPath: path.join(__dirname, ''),  //default path should be existing if it exists
			buttonLabel: 'Export',
			// Restricting the user to only Collision Box Generator Files.
			filters: [
				{
					name: 'Entity Data Files',
					extensions: ['data']
				}, ],
			properties: []
		}).then(file => {
			// Stating whether dialog operation was cancelled or not.
			console.log(file.canceled);
			if (!file.canceled) {
				console.log(file.filePath.toString());
				
				hitboxGeneratorUtility.exportFilePath = file.filePath.toString();
				  
				hitboxGeneratorUtility.export();
			}
		}).catch(err => {
			console.log(err)
		});
	},
	
	export : function() {
		//create esport file
		let exportJson = hitboxGeneratorUtility.collisionBoxCanvasHandler.createExportJson();
		
		if (hitboxGeneratorUtility.exportFilePath)
		{				  
			// Creating and Writing to the sample.txt file
			fs.writeFile(hitboxGeneratorUtility.exportFilePath, exportJson, function (err) {
				if (err) throw err;
				console.log('Export!');
			});
		}

		else
		{
			console.log(exportJson);
			throw "ERROR! NO EXPORT PATH FOUND!!";
		}		
	},
	
	showDialog : function()
	{
		let options = {properties:["openDirectory"]}

		//Synchronous
		let filePaths = dialog.showOpenDialog(WIN, options).then( (selectedFilePromise)=> {
			let path = selectedFilePromise.filePaths[0];
			this.onSpriteFolderSelected(path);
		});
	},
	
	
	onSpriteFolderSelected(path, collisionBoxData = null) {		

		let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

		let onOffsetChanged = (offsetX, offsetY) => {
			//makes sure that if the offset is changed the input boxes reflect the correct value
			$('#offsetX').val(offsetX);
			$('#offsetY').val(offsetY);
		}
		
		let onBoundingBoxChanged = (boundingBox) => {
			//update the width and height controls 
			$('#boundsWidth').val(boundingBox.width());
			$('#boundsHeight').val(boundingBox.height());
		};
							
		let onCollisionBoxCreated = (collisionBox, currentAnimationName, currentFrameNumber) => {
			//create corresponding ui component
			
			let id = Number(new Date());
			
			//add id collisionBox
			collisionBox.setAttr('collisionBoxId', id);
			
			let collapseButtonId = "collapseButton-" + id;
			let dataTarget = "#editCollisionBoxCardBody-" + id;
			let editCollisionBoxPanelId = "editCollisionBoxPanel-" + id;
			let editCollisionBoxCardBodyId = "editCollisionBoxCardBody-" + id;
			
			let editCollisionBoxPanel = $('#editCollisionBoxPanelTemplate').clone();
			editCollisionBoxPanel.prop("id", editCollisionBoxPanelId);
			$("[name='collapseButton']", editCollisionBoxPanel).prop("id", collapseButtonId);
			$("[name='collapseButton']", editCollisionBoxPanel).attr("data-target", dataTarget);
			$("[name='editCollisionBoxCardBody']", editCollisionBoxPanel).prop("id", editCollisionBoxCardBodyId);
			
			$("[name='x']", editCollisionBoxPanel).val(collisionBox.x());
			$("[name='y']", editCollisionBoxPanel).val(collisionBox.y());
			$("[name='width']", editCollisionBoxPanel).val(collisionBox.width());
			$("[name='height']", editCollisionBoxPanel).val(collisionBox.height());
			
			$("[name='isHitBox']", editCollisionBoxPanel).prop('checked', collisionBox.getAttr('isHitBox'));
			$("[name='damageType']", editCollisionBoxPanel).val(collisionBox.getAttr('damageType') || "");
			$("[name='isHurtBox']", editCollisionBoxPanel).prop('checked', collisionBox.getAttr('isHurtBox'));
			$("[name='isCustomBox']", editCollisionBoxPanel).prop('checked', collisionBox.getAttr('isCustomBox'));
			$("[name='name']", editCollisionBoxPanel).val(collisionBox.getAttr('name') || "");
			$("[name='isShieldBox']", editCollisionBoxPanel).prop('checked', collisionBox.getAttr('isShieldBox'));
			
			
			//on change handlers for input boxes
			$("[name='x']", editCollisionBoxPanel).change(function() {
				let x = $( this ).val();
				collisionBox.x(x);
			});
			
			$("[name='y']", editCollisionBoxPanel).change(function() {
				let y = $( this ).val();
				collisionBox.y(y);
			});
			
			$("[name='width']", editCollisionBoxPanel).change(function() {
				let width = $( this ).val();
				collisionBox.width(width);
			});
			
			$("[name='height']", editCollisionBoxPanel).change(function() {
				let height = $( this ).val();
				collisionBox.height(height);
			});
			
			//checkboxes for box type
			$("[name='isHitBox']", editCollisionBoxPanel).change(function() {
				if(this.checked) {
					//show the check box form fields
					$("[name='hitBoxFields']", editCollisionBoxPanel).show();
					collisionBox.setAttr('isHitBox', true);

				}
				
				else
				{
					$("[name='hitBoxFields']", editCollisionBoxPanel).hide();
					collisionBox.setAttr('isHitBox', false);
				}
			});
			
			$("[name='isHurtBox']", editCollisionBoxPanel).change(function() {
				if(this.checked) {
					//show the check box form fields
					$("[name='hurtBoxFields']", editCollisionBoxPanel).show();
					collisionBox.setAttr('isHurtBox', true);

				}
				
				else
				{
					$("[name='hurtBoxFields']", editCollisionBoxPanel).hide();
					collisionBox.setAttr('isHurtBox', false);

				}
			});
			
			$("[name='isCustomBox']", editCollisionBoxPanel).change(function() {
				if(this.checked) {
					//show the check box form fields
					$("[name='customBoxFields']", editCollisionBoxPanel).show();
					collisionBox.setAttr('isCustomBox', true);
				}
				
				else
				{
					$("[name='customBoxFields']", editCollisionBoxPanel).hide();
					collisionBox.setAttr('isCustomBox', false);
				}
			});
			
			$("[name='isShieldBox']", editCollisionBoxPanel).change(function() {
				if(this.checked) {
					//show the check box form fields
					$("[name='shieldBoxFields']", editCollisionBoxPanel).show();
					collisionBox.setAttr('isShieldBox', true);

				}
				
				else
				{
					$("[name='shieldBoxFields']", editCollisionBoxPanel).hide();
					collisionBox.setAttr('isShieldBox', false);
				}
			});
			
			//initial collisionBox properties				
			
			//Hitbox specific fields
			$("[name='damage']", editCollisionBoxPanel).change(function() {
				let damage = $(this).val();
				collisionBox.setAttr('damage', damage);
			});
			
			$("[name='attackName']", editCollisionBoxPanel).change(function() {
				let attackName = $(this).val();
				collisionBox.setAttr('attackName', attackName);
			});
			
			$("[name='damageType']", editCollisionBoxPanel).change(function() {
				let damageType = $(this).val();
				collisionBox.setAttr('damageType', damageType);
			});
			
			//Hurtbox specific fields
			
			//ShieldBox specific fields
			
			//CustomBox specific fields
			$("[name='name']", editCollisionBoxPanel).change(function() {
				let name = $(this).val();
				collisionBox.setAttr('name', name);
			});
			
			
			//add functionality to delete
			$('[name="deleteButton"]', editCollisionBoxPanel).click(function() {
				collisionBoxCanvasHandler.deleteCollisionBox(collisionBox);
			});
			
			
			//add functionality to show/hide
			$('[name="showHideToggleButton"]', editCollisionBoxPanel).click(function() {
				//TODO:
				if (collisionBox.isVisible()) {
					//change the button to hidden
					$('[name="eye"]', editCollisionBoxPanel).hide();
					$('[name="eye-slash"]', editCollisionBoxPanel).show();
					
					collisionBox.hide();
				}
				
				else {
					$('[name="eye"]', editCollisionBoxPanel).show();
					$('[name="eye-slash"]', editCollisionBoxPanel).hide();
					collisionBox.show();					
				}
			});			
			
			
			//need to rename the "name" property of frameScope because radio buttons use that to determine group
			let frameScopeRadioName = "frameScope-" + id;
			$('[name="frameScope"]', editCollisionBoxPanel).each(function() {
				$(this).prop('name', frameScopeRadioName);
			});
			
			//add to correct div
			if (currentAnimationName == null) {
				
				$('[name="' + frameScopeRadioName + '"][value="inAllAnimations"]', editCollisionBoxPanel).prop('checked', true);				
				$('#inAllAnimationsPanels').append(editCollisionBoxPanel);
			}
			
			else if (currentFrameNumber == null) {
				$('[name="' + frameScopeRadioName + '"][value="inAllFrames"]', editCollisionBoxPanel).prop('checked', true);				

				let inAllFramesPanelsJquery = hitboxGeneratorUtility.getInAllFramesPanelJquery(currentAnimationName);
				inAllFramesPanelsJquery.append(editCollisionBoxPanel);
				
			}
			
			else {
			
				$('[name="' + frameScopeRadioName + '"][value="inCurrentFrame"]', editCollisionBoxPanel).prop('checked', true);				

				
				hitboxGeneratorUtility.getFramePanelJquery(currentAnimationName, currentFrameNumber).append(editCollisionBoxPanel);
			
			}
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;
			

			//actions for for frameScope radio
			$('[name="' + frameScopeRadioName + '"]', editCollisionBoxPanel).change(function() {
				
				if (this.value == 'inCurrentFrame') {
					collisionBoxCanvasHandler.moveCollisionBoxToCurrentAnimation(collisionBox);
				}
				
				else if (this.value == 'inAllFrames') {
					collisionBoxCanvasHandler.moveCollisionBoxToInAllFramesOnCurrentAnimation(collisionBox); 
				}
				
				else if (this.value == 'inAllAnimations') {
					//move box to inCurrentFrame
					collisionBoxCanvasHandler.moveCollisionBoxToInAllAnimations(collisionBox);					
				}
				
				else {
					alert('ERROR INVALID SELECTION!');
				}
			});
			
		}
		
		let onCollisionBoxChanged = (collisionBox) => {
			//find the corresponding html form, and update its fields
			let panel = hitboxGeneratorUtility.getCorrespondingEditCollisionBoxPanelFromCollisionBox(collisionBox);
			hitboxGeneratorUtility.updatePanelFieldsFromCollisionBox(panel, collisionBox);
		}
		
		let onCollisionBoxDeleted = (collisionBox) => {
			let panel = hitboxGeneratorUtility.getCorrespondingEditCollisionBoxPanelFromCollisionBox(collisionBox);
			panel.remove();
		}
				
		this.setUpAnimationUIPanels(path);

		hitboxGeneratorUtility.collisionBoxCanvasHandler = new CollisionBoxCanvasHandler(path, onOffsetChanged, onBoundingBoxChanged, onCollisionBoxCreated, onCollisionBoxChanged, onCollisionBoxDeleted, collisionBoxData);		

		//let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;

	},
	
	setUpAnimationUIPanels : function(path) {
		let animations = utils.getAnimations(path);
		
		let splitPath = path.split('/');
		let spriteName = splitPath[splitPath.length - 1];
		//store animation info for future info
		hitboxGeneratorUtility.currentSprite = {
			name : spriteName,
			animations : animations
		}
		
		
		//populate animations and frame selects with intial values
		$('#animations').empty();
		let firstAnimationName = null;
		//let hitBoxPanel = $('#hitBoxPanels');
		for (let animationName in animations)
		{
			//assign the firstAnimationName if hasn't been assigned yet
			firstAnimationName = firstAnimationName || animationName;
			console.log("Adding animation " + animationName);
			$('#animations').append('<option value="' + animationName + '">' + animationName + '</option>');

			//create divs in ui panel for animation
			let animationUIPanelDiv = '<div id="animationPanel-' + animationName + '"  name="animationPanel">';
			
			let frames = animations[animationName].frames;
			let frameUIPanelsHTML = '';
			frameUIPanelsHTML += '<div name="inAllFramesPanels" class=""></div><hr>';
			for (let i = 0; i < frames.length; i++)
			{
				let frameUIPanelHTML = '<div name="framePanel-' + i + '" class="framePanel"></div>';
				frameUIPanelsHTML += frameUIPanelHTML;
				
			}
			
			animationUIPanelDiv += frameUIPanelsHTML;
			
			animationUIPanelDiv += '</div>';
			
			
			$('#hitBoxPanels').append(animationUIPanelDiv);
		}
					
		$('#animations').val(firstAnimationName);
		
		
		$('#frames').empty();
		let frames = animations[firstAnimationName].frames;
		for (let i = 0; i < frames.length; i++)
		{
			$('#frames').append('<option value="' + i + '"> ' + frames[i] + '</option>'); 
		}			
		$('#frames').val(0);
	},
	
	resetFramesSelect : function(currentAnimation) {
		$('#frames').empty();
		let frames = currentAnimation.frames;
		for (let i = 0; i < frames.length; i++)
		{
			$('#frames').append('<option value="' + i + '"> ' + frames[i] + '</option>'); 
		}			
		$('#frames').val(0);
	},
	
	setUpAnimationInfoPanel() {
		
		//add on change for fps 
		$('#fps').change(function() {
			let valString = $('#fps').val();
			let val = parseInt(valString);
			if (Number.isNaN(val))
			{
				val = 0;
				$('#fps').val(0);
			}
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;
			
			collisionBoxCanvasHandler.setFrameRateOnCurrentAnimation(val);

		});
		
		//add on change for isLooping
		$('#isLooping').change(function() {
			let valString = $('#isLooping').val();
			let val = (valString === 'true')
			
			let collisionBoxCanvasHandler = hitboxGeneratorUtility.collisionBoxCanvasHandler;
			
			collisionBoxCanvasHandler.setIsLoopingOnCurrentAnimation(val);

		});		
	},
	
	//open the save file and load it to be edited/exported
	openCollisionBoxFile() {
		let options = {properties:["openDirectory"]}

		//Synchronous
		let filePaths = dialog.showOpenDialog(WIN, options).then( (dir)=> {
			//confirm that a collisionBoxData file was selected
			
			//read collisionBoxInfo into obj
			
			//load from dir
			this.onSpriteFolderSelected(spriteDir);
			
			
			
		});
	},
	
	
	setUpSelects : function() {
		
	},
	
	getCollisionBoxId : function(collisionBox) {
		let collisionBoxId = collisionBox.getAttr('collisionBoxId');
		return collisionBoxId;
	},
	
	getCorrespondingEditCollisionBoxPanelFromCollisionBox : function(collisionBox) {
		let collisionBoxId = hitboxGeneratorUtility.getCollisionBoxId(collisionBox);
		let editCollisionBoxPanelId = "editCollisionBoxPanel-" + collisionBoxId;
		return $('#' + editCollisionBoxPanelId);
	},
	
	updatePanelFieldsFromCollisionBox : function(panel, collisionBox) {
		
		console.log('x is ' + collisionBox.x());
		console.log('y is ' + collisionBox.y());
		console.log('width is ' + collisionBox.width());
		console.log('height is ' + collisionBox.height());
		
		$('[name="x"]', panel).val(collisionBox.x());
		$('[name="y"]', panel).val(collisionBox.y());
		$('[name="width"]', panel).val(collisionBox.width());
		$('[name="height"]', panel).val(collisionBox.height());

	},
	
	hideAllFramePanels : function(animationName) {
		let animationPanel = this.getAnimationPanelJQuery(animationName);

		$('[name="framePanel"]', animationPanel).each(function() {
			$(this).hide();
		});
	},
	
	showFramePanel : function(animationName, frameNumber) {
		//hide panels for all animations
		hitboxGeneratorUtility.hideAllAnimationPanels();

		let animationPanel = this.getAnimationPanelJQuery(animationName);
		animationPanel.show();

		this.hideAllFramePanels(animationName);
		
		//show inAllFrames
		let inAllFrames = this.getInAllFramesPanelJquery(animationName);
		inAllFrames.show();
		
		//show the panels of current frame
		let framePanel = this.getFramePanelJquery(animationName, frameNumber);
		framePanel.show();
		
	},
	
	hideAllAnimationPanels : function() {
		$('[name="animationPanel"]').each(function() {
			$(this).hide();
		});
	},
	
	getAnimationPanelJQuery : function(animationName) {
		let animationPanelId = 'animationPanel-' + animationName;
		let animationPanel = $('#' + animationPanelId);
		return animationPanel;
	},
	
	getInAllFramesPanelJquery : function(animationName) {
		let animationPanel = hitboxGeneratorUtility.getAnimationPanelJQuery(animationName);
		return $('[name="inAllFramesPanels"]', animationPanel);		
	},
	
	getFramePanelJquery : function(animationName, frameNumber) {
		let animationPanel = hitboxGeneratorUtility.getAnimationPanelJQuery(animationName);
		return $('[name="framePanel-' + frameNumber + '"]', animationPanel);
	},
	
	getAllFramePanelsJquery : function(animationName) {
		let currentAnimationPanel = hitboxGeneratorUtility.getAnimationPanelJQuery(animationName);
		return $('.framePanel', currentAnimationPanel);
	},
	
	hideAllFramePanels : function(animationName) {
		let framePanels = hitboxGeneratorUtility.getAllFramePanelsJquery(animationName);
		framePanels.each(function() {
			$(this).hide();
		});
	},
	
	showFrame : function(animationName, frameNumber) {
		hitboxGeneratorUtility.hideAllFramePanels();
		let framePanel = this.getFramePanelJquery(animationName, frameNumber);
		framePanel.show();		
	}
}