module.exports = {

    getAnimations(path) {
		
		let animationFolders = fs.readdirSync(path);
		
		let animations = {};
		animationFolders.forEach((animationFolderName) => {
			console.log(animationFolderName);
			let animationPath = path + "/" + animationFolderName;				
			
			let frames = fs.readdirSync(animationPath);
			let framesImageNames = [];
			frames.forEach((frame) => {
				framesImageNames.push(frame);
			});
			
			let animation = {
				name : animationFolderName,
				path : animationPath,
				frames : framesImageNames
			};

			//create animation obj
			animations[animationFolderName] = animation;
			
		});
		
		return animations;
    }
}