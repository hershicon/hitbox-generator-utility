/***
*
* wrapper class around kanvajs to control/enforce a specific kanvajs structure
*
***/

const fs = require('fs')
const utils = require('./utils.js')

class CollisionBoxCanvasHandler {
	
	path;
	currentAnimationName;
	currentFrameNumber;
	stage;
	transformer;
	centeredTransformer;
	canvasWidth;
	canvasHeight;
	
	canvasWidth = 800;
	canvasHeight = 800;
	
	initialBoundingBoxWidth = 50;
	initialBoundingBoxHeight = 50;
	
	onOffsetChanged;
	onBoundingBoxChanged;
	onCollisionBoxCreated;
	onCollisionBoxChanged;
	onCollisionBoxDeleted;
	
	offsetX = 0;
	offsetY = 0;
	
	constructor(path, onOffsetChanged, onBoundingBoxChanged, onCollisionBoxCreated, onCollisionBoxChanged, onCollisionBoxDeleted, collisionBoxSaveData = null) {
		this.stage = new Konva.Stage({
		  container: 'container',   // id of container <div>
		  width: this.canvasWidth,
		  height: this.canvasHeight
		});
		
		let stage = this.stage;
		let collisionBoxCanvasHandlerForClosure = this;
		
		stage.on('click', function (e) {
			// e.target is a clicked Konva.Shape or current stage if you clicked on empty space
			console.log('clicked on', e.target);
			console.log(
			  'usual click on ' + JSON.stringify(stage.getPointerPosition())
			);
			
			//if empty space is clicked, then deselect
			if (e.target == stage)
			{
				collisionBoxCanvasHandlerForClosure.deselectAllBoxes();
			}
		});
		
		this.path = path;
		
		//assign callbacks
		this.onOffsetChanged = onOffsetChanged;
		this.onBoundingBoxChanged = onBoundingBoxChanged;
		this.onCollisionBoxCreated = onCollisionBoxCreated;
		this.onCollisionBoxChanged = onCollisionBoxChanged;
		this.onCollisionBoxDeleted = onCollisionBoxDeleted;
		
		
		this.loadAnimations(path);
		
		if (collisionBoxSaveData != null) {	
			this.populateCollisionBoxSaveData(collisionBoxSaveData);
		}
		
		this.addKeyboardHandlers(stage);
		
	}
	
	addOnBoundingBoxChangedHandler(onBoundingBoxChanged) {
		this.onBoundingBoxChanged = onBoundingBoxChanged;
	}
	
	addOnOffsetChanged(onOffsetChanged) {
		this.onOffsetChanged = onOffsetChanged;
	}

	addOnCollisionBoxCreatedHandler(onCollisionBoxCreated) {
		this.onCollisionBoxCreated = onCollisionBoxCreated;
	}
	
	addOnCollisionBoxChangedHandler(onCollisionBoxChanged) {
		console.log('added on collisionBoxChanged');
		this.onCollisionBoxChanged = onCollisionBoxChanged;
	}
		
	addOnCollisionBoxDeletedHandler(onCollisionBoxDeleted)
	{
		this.onCollisionBoxDeleted = onCollisionBoxDeleted;
	}
	
	/**
	*
	* @param collisionBox - a json structure representing collisionBoxInformation 
	* @param animationName - the name of the animation you wish to add to. If null means this is in all animations
	* @param frameNumber - a number indicating which frame (starting with first frame at 0), if null (but animationName is not null), then it is in all frames (though IN_ALL_ANIMATIONS takes precedence)
	*
	**/
	createCollisionBox(collisionBoxJson = null, animationName = this.currentAnimationName, frameNumber = this.currentFrameNumber) {
				
		const { 
			x = 0,
			y = 0,
			width = 50,
			height = 50,
			isHitBox = false,
			attackName = null,
			damage = null,
			damageType = null,
			isHurtBox = false,
			isCustomBox = false,
			name = null,
			isShieldBox = false

		} = collisionBoxJson || {};
				
		let collisionBoxGroup = this.getCollisionBoxesGroup(animationName, frameNumber);
				
		let collisionBoxCanvasHandlerForClosure = this;
		let rect = new Konva.Rect({
		  x: x,
		  y : y,
		  width: width,
		  height: height,
		  fillEnabled: true,
		  fill: 'rgba(255, 0, 0, .1)',
		  stroke: 'red',
		  strokeWidth: 2,
		  draggable: true,

		});
		
		rect.setAttr("isHitBox", isHitBox);
		rect.setAttr("attackName", attackName);
		rect.setAttr("damage", damage);
		rect.setAttr("damageType", damageType);
		rect.setAttr("isHurtBox", isHurtBox);
		rect.setAttr("isCustomBox", isCustomBox);
		rect.setAttr("name", name);
		rect.setAttr("isShieldBox", isShieldBox);
		
		rect.on('click tap',  function(e) {
			console.log('click detected');
			let transformer = collisionBoxCanvasHandlerForClosure.getTransformer();
			transformer.nodes([this]);
			transformer.show();
			collisionBoxCanvasHandlerForClosure.stage.draw();
		});
		
		rect.on('transformend',  function()  {
			console.log('transform end');
			//Konva transformer changes scale rather than width and height, so need to adjust width and height after transform is complete. 
			
			collisionBoxCanvasHandlerForClosure.resetScaleOnCollisionBox(this);			
			
			collisionBoxCanvasHandlerForClosure.onCollisionBoxChanged && collisionBoxCanvasHandlerForClosure.onCollisionBoxChanged(this);			
		 });
		 
		rect.on('dragmove', function () {
			//Konva transformer changes scale rather than width and height, so need to adjust width and height after transform is complete. 
			console.log('dragmove');
			
			collisionBoxCanvasHandlerForClosure.resetScaleOnCollisionBox(this);			
			
			collisionBoxCanvasHandlerForClosure.onCollisionBoxChanged && collisionBoxCanvasHandlerForClosure.onCollisionBoxChanged(this);
		});			
		 
		collisionBoxGroup.add(rect);
		
		this.onCollisionBoxCreated && this.onCollisionBoxCreated(rect, animationName, frameNumber);		
	}
	
	loadAnimations(path) {
					
		let animations = utils.getAnimations(path);
				
		//populate select with animation name 
		let splitPath = path.split('/');
		let spriteName = splitPath[splitPath.length - 1];
		this.currentSprite = {
			name : spriteName,
			animations : animations
		}

		//select the first animation as the default
		for (let animationName in this.currentSprite.animations)
		{
			this.currentAnimationName =  animationName;
			break;
		}
		
		this.currentFrameNumber = 0; 
		
		
		this.resetFramesSelect();		
					
		//createAnimationLayers
		this.createKonvaAnimationStructure(path);
		
		//show default selected frame
		this.showFrame(this.currentAnimationName, this.currentFrameNumber);
		
	}
	
	populateCollisionBoxSaveData(collisionBoxSaveData) {
		let boundsJson = collisionBoxSaveData['bounds'];
		
		//set offset
		this.setOffsetX(boundsJson.offsetX);
		this.setOffsetY(boundsJson.offsetY);
		
		if (this.onOffsetChanged) {
			this.onOffsetChanged(this.offsetX, this.offsetY);
		}
		
		this.setBoundingBoxDimensions(boundsJson.width, boundsJson.height);
		
		let animationsJson = collisionBoxSaveData['animations'];
		
		for (let animation in animationsJson)
		{
			//special case
			if (animation === "collisionBoxesForAllAnimations") {
				let collisionBoxes = animationsJson[animation];
				for (let i = 0; i < collisionBoxes.length; i++) {
					let collisionBoxJson = collisionBoxes[i];							
					this.createCollisionBox(collisionBoxJson, null, null);
				}				
			}
			
			else {
				let animationJson = animationsJson[animation];
				let frames = animationJson['collisionBoxes'];
				
				for (let frame in frames) {
					
					if (frame === "inAllFrames") {
						let collisionBoxes = frames[frame];
						for (let i = 0; i < collisionBoxes.length; i++) {
							let collisionBoxJson = collisionBoxes[i];							
							this.createCollisionBox(collisionBoxJson, animation, null);
						}
					}
					
					else {						
						let collisionBoxes = frames[frame];
						
						for (let i = 0; i < collisionBoxes.length; i++) {
							let collisionBoxJson = collisionBoxes[i];
							
							this.createCollisionBox(collisionBoxJson, animation, frame);
						}
					}
				}
			}
		}			
	}
	
	
	
	getTransformer() {
		return this.transformer;
	}
	
	
	//structure is :
	//stage
	//  contentLayer
	//    boundingBoxLayer
	//      boundingBox (rect)
	//    animationsLayer
	//      inAllAnimationsGroup
	//        COLLISION_BOX_1 (Rect)
	//        COLLISION_BOX_2 (Rect)
	//        ...
	//      ANIMATION_1
	//        inAllFramesGroup
	//          COLLISION_BOX_1 (Rect)
	//          COLLISION_BOX_2 (Rect)
	//          ...
	//		  FRAME_1
	//          COLLISION_BOX_1 (Rect)
	//          COLLISION_BOX_2 (Rect)
	//          ...
	//        FRAME_2
	//      ANIMATION_2
	//        inAllFramesGroup
	//          COLLISION_BOX_1 (Rect)
	//          COLLISION_BOX_2 (Rect)
	//          ...
	//        FRAME 1
	//          COLLISION_BOX_1 (Rect)
	//          COLLISION_BOX_2 (Rect)
	//          ...
    //      ...
	//    imageGroup
	//      image of frame
	//    transformerLayer
	//       centeredTransformer (Transformer for boundingBox)
	//
	createKonvaAnimationStructure(path) {
		
		let contentLayer = new Konva.Layer({name: "contentLayer"});
		this.stage.add(contentLayer);
		//center content layer such that 0,0 is the middle of the canvas
		contentLayer.absolutePosition({x:this.canvasWidth / 2, y: this.canvasHeight / 2});
		
		//create image group
		let imageGroup = new Konva.Group({name: "image-group"});
		contentLayer.add(imageGroup);
		
		//add top level animations layer
		let animationsLayer = new Konva.Group({name: "animationsLayer"});
		contentLayer.add(animationsLayer);

		let transformerLayer = new Konva.Group({name: "transformerLayer"});
		contentLayer.add(transformerLayer);
		let tr = new Konva.Transformer({rotateEnabled: false, keepRatio: false, flipEnabled: false});		
		this.transformer = tr;
		transformerLayer.add(tr);
		
		//Add Group for ALL_ANIMATIONS
		let inAllAnimationsGroup = new Konva.Group({
			name : 'inAllAnimations',
			groupType : 'inAllAnimations'
		});
		animationsLayer.add(inAllAnimationsGroup);

		this.setUpBoundingBoxKonvaStructure(contentLayer, transformerLayer);

		this.setUpAnimationsKonvaStructure(animationsLayer);
			
		this.createAndAddGuideLayer();
				
		this.stage.draw();

		
	}
	
	setUpBoundingBoxKonvaStructure(contentLayer, transformerLayer) {
		
		//add top level animations layer
		let boundingBoxLayer = new Konva.Group({name: "boundingBoxLayer"});
		contentLayer.add(boundingBoxLayer);
		
		let canvasWidthMidpoint = this.canvasWidth / 2;
		let canvasHeightMidpoint = this.canvasHeight / 2;

		//reprents the bounding box object, will not be able to change the position of this through dragging or resizing as it should be deliberate

		let boundingBox = new Konva.Rect({
			x: -this.initialBoundingBoxWidth / 2,
			y: -this.initialBoundingBoxHeight / 2,
			width: this.initialBoundingBoxWidth,
			height: this.initialBoundingBoxHeight,
			name: 'boundingBox',
		    fillEnabled: true,
		    fill: 'rgba(0, 255, 0, .1)',
		    stroke: 'blue',
		    strokeWidth: 2,
		    draggable: false,

		});
		
		
		let centeredTransformer = new Konva.Transformer({rotateEnabled: false, keepRatio: false, centeredScaling: true, flipEnabled: false });
		transformerLayer.add(centeredTransformer);
		this.centeredTransformer = centeredTransformer;
		
		let collisionBoxCanvasHandlerForClosure = this;
		
		boundingBox.on('click tap',  function(e) {
			console.log('click detected');
			centeredTransformer.nodes([boundingBox]);
			centeredTransformer.show();
			collisionBoxCanvasHandlerForClosure.stage.draw();
		});
		
		boundingBox.on('transformend',  function() {
			console.log('transform end');
			//Konva transformer changes scale rather than width and height, so need to adjust width and height after transform is complete. 			
			//TODO make the new widths round to nearest pixel
			collisionBoxCanvasHandlerForClosure.resetScaleOnCollisionBox(this);
			
			collisionBoxCanvasHandlerForClosure.onBoundingBoxChanged && collisionBoxCanvasHandlerForClosure.onBoundingBoxChanged(this);
			
		 });
		
		
		boundingBoxLayer.add(boundingBox);
	}

	setUpAnimationsKonvaStructure(animationsLayer) {
		
		for (let animationName in this.currentSprite.animations)
		{
			let animation = this.currentSprite.animations[animationName];
			
			let animationGroup = new Konva.Group({
				name : animationName,
				groupType : 'animation',
				frameRate : 12,
				isLooping : false
			});
			
			let inAllFramesGroup = new Konva.Group({
				name: 'inAllFrames',
				groupType: 'frame'
			});
			
			inAllFramesGroup.visible(false);			
			animationGroup.add(inAllFramesGroup);
						
			for (let i = 0; i < animation.frames.length; i++)
			{
				let frameGroup = new Konva.Group({
					name: ""+i,
					groupType: 'frame'
				});
				frameGroup.visible(false);
				animationGroup.add(frameGroup);								
			}
			
			animationsLayer.add(animationGroup);			
		}
	}
	
	createAndAddGuideLayer() {
		let guideLayer = new Konva.Layer({name: "guideLayer"});
		this.stage.add(guideLayer);
		
		let canvasWidth = this.canvasWidth;
		let canvasHeight = this.canvasHeight;
		
		let verticalGuide = new Konva.Line({
			points: [canvasWidth/2, 0, canvasWidth/2, canvasHeight],
			stroke: 'black',
			strokeWidth: 1,
		});
		guideLayer.add(verticalGuide);
		
		let horizontalGuide = new Konva.Line({
			points: [0, canvasHeight/2, canvasWidth, canvasHeight/2],
			stroke: 'black',
			strokeWidth: 1,
		});
		guideLayer.add(horizontalGuide);
	}
	
	showFrame(animationName, frameNumber) {

		let frameNumberClass = "." + frameNumber;
		
		
		//hide all frames
		this.hideAllAnimationsAndFrames();
		
		//show in all frames for this animation, which shouldn't be hidden regardeless of what frame of the animation is shown
		let inAllFramesGroup = this.getInAllFramesGroup(animationName);
		inAllFramesGroup.show();
		
		//show selected animation group
		let animationGroup = this.getAnimationGroup(animationName);

		animationGroup.show();
		
		
		//show frame image
		animationGroup.findOne(frameNumberClass).show();
		
		
		//load image for that frame
		this.loadImage(animationName, frameNumber);
		
		//save current frame and animation
		this.currentAnimationName = animationName;
		this.currentFrameNumber = frameNumber;
				
		this.deselectAllBoxes();

	}
	
	loadImage(animationName, frameNumber) { 
		//delete old image
		let imageGroup = this.getImageGroup();
		imageGroup.destroyChildren();
		
		//add new one
		let animation = this.currentSprite.animations[animationName];
		let imagePath = this.path + "/" + animationName + "/" + animation.frames[frameNumber];
		
		let collisionBoxCanvasHandlerForClosure = this;
		
		
		Konva.Image.fromURL(imagePath,  (sprite) => {
			
			sprite.setAttrs({
			  x: - sprite.width() / 2,
			  y: - sprite.height() / 2,
			  scaleX: 1,
			  scaleY: 1,
			  stroke: 'black',
			  name : 'frame-image'
			});
			
			//TODO: Need to only add sprite if loaded
			imageGroup.add(sprite);
			
			collisionBoxCanvasHandlerForClosure.updateImagePositionBasedOnOffset();
		});
	}
	
	setOffsetX(x)
	{
		this.offsetX = x;
		this.updateImagePositionBasedOnOffset();
	}
	
	setOffsetY(y)
	{
		this.offsetY = y;
		this.updateImagePositionBasedOnOffset();		
	}
	
	updateImagePositionBasedOnOffset()
	{
		let imageGroup = this.getImageGroup();
		imageGroup.position({x: this.offsetX, y: this.offsetY});
	}
	
	
	getImageGroup()
	{
		return this.getContentLayer().findOne('.image-group');
	}
	
	hideAllAnimationsAndFrames()
	{
		let animationGroups = this.getAnimationGroups();
		for (let index in animationGroups)
		{
			let animationGroup = animationGroups[index];
			//hideFramesByAnimation
			this.hideFramesByAnimation(animationGroup);
			//hide this animation
			animationGroup.hide();
			
		}
	}
	
	hideFramesByAnimation(animationGroup)
	{
		let frames = animationGroup.getChildren((node)=>{
		   return node.getAttr('groupType') === 'frame';
		});

		for (let index in frames)
		{
			let frame = frames[index];
			frame.hide();
		}
	}
	
	getContentLayer()
	{
		return this.stage.findOne('.contentLayer');
	}
	
	getBoundingBoxGroup()
	{
		let contentLayer = this.getContentLayer();
		let boundingBoxGroup = contentLayer.findOne('.boundingBoxLayer');
		return boundingBoxGroup;
	}
	
	getBoundingBox()
	{
		let boundingBox = this.getBoundingBoxGroup().findOne('.boundingBox');
		return boundingBox;
	}
	
	setBoundingBoxDimensions(width, height) {
		let boundingBox = this.getBoundingBox();
		boundingBox.x(-width/2); //reset to keep centered
		boundingBox.y(-height/2); //reset to keep centered
		boundingBox.width(width);
		boundingBox.height(height);
	}
	
	getCurrentImage()
	{
		let image = this.getImageGroup().findOne('.frame-image');
		return image;
	}
	
	getAnimationGroups()
	{
		let animationGroups = this.getAnimationsLayer().getChildren((node)=>{
		   return node.getAttr('groupType') === 'animation';
		});
		
		return animationGroups;
	}
		
	getAnimationGroup(animationName)
	{
		let animationClass = "." + animationName;
		return this.getAnimationsLayer().findOne(animationClass);
	}
	
	getFrameRateOnCurrentAnimation() {
		let animationGroup = this.getAnimationGroup(this.currentAnimationName);
		return animationGroup.getAttr('frameRate');
	}
	
	setFrameRateOnCurrentAnimation(fps) {
		let animationGroup = this.getAnimationGroup(this.currentAnimationName);
		animationGroup.setAttr('frameRate', fps);
	}
	
	getIsLoopingOnCurrentAnimation() {
		let animationGroup = this.getAnimationGroup(this.currentAnimationName);
		return animationGroup.getAttr('isLooping');
	}
	
	
	
	setIsLoopingOnCurrentAnimation(isLooping) {
		let animationGroup = this.getAnimationGroup(this.currentAnimationName);
		animationGroup.setAttr('isLooping', isLooping);
	}
	
	//group that contains boxes for all animations
	getInAllAnimationsGroup()
	{
		let inAllAnimationsClass = '.inAllAnimations';
		
		let inAllAnimationsGroup = this.getAnimationsLayer().findOne(inAllAnimationsClass);
	
		return inAllAnimationsGroup;
	
	}
	
	getFrameGroup(animationName, frameNumber)
	{
		let frameNumberClass = "." + frameNumber;
		let frameGroup = this.getAnimationGroup(animationName).findOne(frameNumberClass);
		return frameGroup;
	}
	
	getInAllFramesGroup(animationName)
	{
		let animationGroup = this.getAnimationGroup(animationName);
		let inAllFramesClass = '.inAllFrames';
		let inAllFramesGroup = animationGroup.findOne(inAllFramesClass);
		return inAllFramesGroup;
	}
	
	deleteCollisionBox(collisionBox) {
		collisionBox.remove();
		this.onCollisionBoxDeleted(collisionBox);
	}
	
	moveCollisionBoxToCurrentAnimation(collisionBox)
	{
		this.deleteCollisionBox(collisionBox);
		let collisionBoxJson = this.convertCollisionBoxToJson(collisionBox);		
		this.createCollisionBox(collisionBoxJson, this.currentAnimationName, this.currentFrameNumber);
	}

	moveCollisionBoxToInAllFramesOnCurrentAnimation(collisionBox) {
		this.deleteCollisionBox(collisionBox);
		let collisionBoxJson = this.convertCollisionBoxToJson(collisionBox);		
		this.createCollisionBox(collisionBoxJson, this.currentAnimationName, null);		
	}
	
	moveCollisionBoxToInAllAnimations(collisionBox) 
	{				
		this.deleteCollisionBox(collisionBox);
		let collisionBoxJson = this.convertCollisionBoxToJson(collisionBox);		
		this.createCollisionBox(collisionBoxJson, null, null);		
	}
			
	/*
	*
	* @param animationName if null, represents inAllAnimations group
	* @param frameNumber if null but animationName exists, represents inAllFrames of that corresponding animation
	*/
	
	getCollisionBoxesGroup(animationName, frameNumber)
	{		
		if (animationName == null) 
		{
			return this.getInAllAnimationsGroup();
		}
		
		if (frameNumber == null) 
		{
			return this.getInAllFramesGroup(animationName);
		}
		
		let frameGroup = this.getFrameGroup(animationName, frameNumber);
				
		return frameGroup;
	}
	
	getAnimationsLayer()
	{
		return this.stage.findOne('.animationsLayer');
	}
	
	getCurrentFrameGroup()
	{
		return this.getFrameGroup(this.currentAnimationName, this.currentFrameNumber);
	}
	
	deselectAllBoxes()
	{
		this.transformer.nodes([]);
		this.centeredTransformer.nodes([]);

	}
		
	resetFramesSelect() {		
		this.currentFrameNumber = 0;			
	}
	
	resetScaleOnCollisionBox(collisionBox) {
		
		collisionBox.width(Math.round(collisionBox.width() * collisionBox.scaleX()));
		collisionBox.height(Math.round(collisionBox.height() * collisionBox.scaleY()));
		collisionBox.scaleX(1);
		collisionBox.scaleY(1);
		
		//round x, y, width, height to int values
		collisionBox.x(Math.round(collisionBox.x()));
		collisionBox.y(Math.round(collisionBox.y()));
		collisionBox.width(Math.round(collisionBox.width()));
		collisionBox.height(Math.round(collisionBox.height()));

		
	}
	
	//THIS IS THE SAVE FILE! NOT THE EXPORTED ONE FOR USE IN GAME
	createSaveJson(includePath=true) {
		let saveJson = {};
		let animationGroups = this.getAnimationGroups();
		let splitPath = this.path.split('/');
		let spriteName = splitPath[splitPath.length - 1];
		
		let boundingBox = this.getBoundingBox();
		let bounds = {
			offsetX: this.offsetX,
			offsetY: this.offsetY,
			width: boundingBox.width(),
			height: boundingBox.height()
		};
		
		let animations = {
			
		};
		
		console.log(this.path);
		if (includePath) {
			saveJson['path'] = this.path;
		}
		
		for (let i = 0; i < animationGroups.length; i++)
		{
			let animationGroup = animationGroups[i];
			/*
			    "idle": {
				  "collisionBoxes": {},
				  "frameRate": 3.0,
				  "isLooping": false,
				  "customAnimationName": null
				},
			*/
			
			let collisionBoxesJson = {};
			
			let frameGroups = animationGroup.getChildren();
			for (let i = 0; i < frameGroups.length; i++)
			{
				let frameGroup = frameGroups[i];
				let frameName = frameGroup.getAttr('name');
				let collisionBoxForFrameJsonArray = [];
				
				//get all collision boxes from here
				//let collisionBoxesGroup = this.getCollisionBoxesGroupByFrameGroup(frameGroup);
				
				let boxes = frameGroup.getChildren();
				for (let j = 0; j < boxes.length; j++)
				{
					let box = boxes[j];
					
					let boxJson = this.convertCollisionBoxToJson(box);

					//add to array
					collisionBoxForFrameJsonArray.push(boxJson);
				}
				
				let frameNumber = frameGroup.getAttr('name');
				//add to collisionBox
				collisionBoxesJson[frameNumber] = collisionBoxForFrameJsonArray;
			}
			
			
			
			
			let frameRate = animationGroup.getAttr('frameRate') || 12;
			let isLooping = animationGroup.getAttr('isLooping') || false;
			let customAnimationName = animationGroup.getAttr('customAnimationName') || null;
			
			let animationName = animationGroup.getAttr('name');
			
			animations[animationName] =  {
				collisionBoxes : collisionBoxesJson,
				frameRate : frameRate,
				isLooping : isLooping,
				customAnimationName : customAnimationName				
			};
		}
		
		//add inAllAnimations
		let inAllAnimationsJsonArray = [];
		let inAllAnimationsGroup = this.getInAllAnimationsGroup();
		let boxes = inAllAnimationsGroup.getChildren();
		for (let j = 0; j < boxes.length; j++)
		{
			let box = boxes[j];
			
			let boxJson = this.convertCollisionBoxToJson(box);

			//add to array
			inAllAnimationsJsonArray.push(boxJson);
		}
		animations['collisionBoxesForAllAnimations'] = inAllAnimationsJsonArray;
		
		
		saveJson['name'] = this.spriteName;
		saveJson['bounds'] = bounds;
		saveJson['animations'] = animations;
		saveJson['faction'] = "Enemy";
		
		console.log(saveJson);
		
		let saveJsonString = JSON.stringify(saveJson, null, 2);
		
		console.log(saveJsonString);
		
		return saveJsonString;
	}
	
	createExportJson() {
		let exportJson = this.createSaveJson(false);
		console.log("---------")
		console.log(exportJson);
		console.log("---------")
		return exportJson;
	}
	
	convertCollisionBoxToJson(box) 
	{
		let boxJson = {};
							
		let isHitBox = box.getAttr('isHitBox');
		let isHurtBox = box.getAttr('isHurtBox');
		let isCustomBox = box.getAttr('isCustomBox');
		let isShieldBox = box.getAttr('isShieldBox');
		
		
		boxJson['isHitBox'] = isHitBox || false;
		boxJson['damage'] = box.getAttr('damage') || 5;
		boxJson['attackName'] = box.getAttr('attackName') || null;
		boxJson['damageType'] = null;
		
		boxJson['isHurtBox'] = isHurtBox || false;
		
		boxJson['isCustomBox'] = isCustomBox || false;
		boxJson['name'] = box.getAttr('name') || null;
		
		boxJson['isShieldBox'] = isShieldBox || false;
		
		//rest of box info 
		boxJson['x'] = box.getAttr('x');
		boxJson['y'] = box.getAttr('y');
		boxJson['width'] = box.getAttr('width');
		boxJson['height'] = box.getAttr('height');
		
		console.log("--Creating boxJson--");
		console.log(boxJson);
		console.log("--Creating boxJson (END) --");

		return boxJson;
	}
	
	hideCollisionBox(collisionBox)
	{
		collisionBox.hide();
		this.transformer.nodes().remove(collisionBox);
	}
	
	addKeyboardHandlers(stage)
	{
		stage.on('keydown', function (e) {
			//DELETE
			if (e.keyCode === 46) {
			  //delete all selected hitboxes
			  tr.nodes().each(function(rect) {
				  deleteCollisionBox(rect);
			  });
			}
		});
	}

	
}
module.exports = CollisionBoxCanvasHandler;
